﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Utils;

namespace Managers {

  public class RoundAnnouncer: MonoBehaviour
  {
      public AudioClip screamsfx;
      private const float ROUND_TIME = 6.0f;
      private float IDLE_TIME => screamsfx.length;
      private List<Action<Round>> _subscriberList = new List<Action<Round>>();
      private float currentTime = 0.0f;
      private Round currentRound;

      private bool broadcasted = false;

      // Start is called before the first frame update
      void Start() {
        StartNextRound();
      }

      IEnumerator waitAndStartRound(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        StartNextRound();
      }
      
      private void StartNextRound() {
        broadcasted = true;
        currentRound = RoundCycle.NextRound();
        currentTime = (currentRound == Round.IDLE) ? IDLE_TIME : ROUND_TIME;
        StartCoroutine(waitAndStartRound(currentTime));
        notifySubscribers(currentRound);
      }

      public void subscribe(Action<Round> notifyWith) {
        _subscriberList.Add(notifyWith);
        
        if(broadcasted)
          notifyWith(currentRound);
      }

      protected void notifySubscribers(Round r) {
        foreach(var subscriber in _subscriberList) {
          subscriber(r);
        }
      }
  }

  class RoundCycle {
    static Round[] roundCycleList = {Round.BREAK, Round.IDLE, Round.REPAIR, Round.IDLE};

    public static int currentRoundIndex = 0;

    public static Round NextRound() {
      Round r = roundCycleList[currentRoundIndex]; 
      currentRoundIndex++;
      if(currentRoundIndex >= roundCycleList.Length) currentRoundIndex = 0;

      return r;
    }
  }

  public enum Round {
    BREAK,
    REPAIR,
    IDLE
  }

  // public class RoundAnnouncerSubscribers
}