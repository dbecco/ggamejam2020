using Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Managers {
  
public class InputWatcher: MonoBehaviour {

    private DualShock4Input[] _inputList =  (DualShock4Input[]) Enum.GetValues(typeof(DualShock4Input));
    private List<InputSubscriber> _subscriberList = new List<InputSubscriber>();

    void Update() {
      foreach(var element in _subscriberList){
        var input = GetPressedInput(element._TAG);
        if(input != DualShock4Input.NONE && element.inFilters(input)) 
          element._subscriberFunc(input);
      }
    }

    public void subscribe(string TAG, Action<DualShock4Input> executable) {
      _subscriberList.Add(new InputSubscriber(TAG, executable));
    }


    public void subscribe(string TAG, DualShock4Input[] filters, Action<DualShock4Input> executable) {
       _subscriberList.Add(new InputSubscriber(TAG, executable, filters));
    }

    private DualShock4Input GetPressedInput(string tag)
    {

        if (Input.GetButtonDown(DualShock4Input.SQUARE.withTag(tag)))
        {
            return DualShock4Input.SQUARE;
        }

        if (Input.GetButtonDown(DualShock4Input.CIRCLE.withTag(tag)))
        {
            return DualShock4Input.CIRCLE;
        }
        
        if (Input.GetButtonDown(DualShock4Input.TRIANGLE.withTag(tag)))
        {
            return DualShock4Input.TRIANGLE;
        }

        if (Input.GetButtonDown(DualShock4Input.CROSS.withTag(tag)))
        {
            return DualShock4Input.CROSS;
        }

        if (Input.GetButtonDown(DualShock4Input.R1.withTag(tag)))
        {
            return DualShock4Input.R1;
        }

        return DualShock4Input.NONE;
    }

  }


  class InputSubscriber {
    public string _TAG;
    public Action<DualShock4Input> _subscriberFunc;
    public DualShock4Input[] _filters = null;

    public InputSubscriber(string TAG, Action<DualShock4Input> subscriberFunc, DualShock4Input[] filters) {
      _TAG = TAG;
      _subscriberFunc = subscriberFunc;
      _filters = filters;
    }
    public InputSubscriber(string TAG, Action<DualShock4Input> subscriberFunc) {
      _TAG = TAG;
      _subscriberFunc = subscriberFunc;
    }

    public bool inFilters(DualShock4Input input) {
      if(null == _filters) return true;

      return Array.Exists(_filters, element => element == input);
    }
  }

}