﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableItem : MonoBehaviour
{
    public Sprite fixedSprite;
    public Sprite brokenSprite;

    private SpriteRenderer _renderer;
   
    // Start is called before the first frame update    
    void Start()
    {
      _renderer = GetComponent<SpriteRenderer>();
    }

    public void SetState(bool broken) {
      if(broken)
        _renderer.sprite = brokenSprite;
      else
        _renderer.sprite = fixedSprite;
    }
}
