using Assets.Scripts.Utils;
using Prime31;
using UnityEngine;
using Utils;
using Managers;
using System;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    // sprite ref
    SpriteRenderer renderer;
    //player parameters
    public CharacterController2D.CharacterCollisionState2D flags;
    public float walkSpeed = 6.0f;
    public float jumpSpeed = 16.0f;
    public float gravity = 20.0f;
    public float doubleJumpSpeed = 10.0f;
    public float wallJumpXPower = 1.5f;
    public float wallJumpYPower = 0.5f;
    public bool isInputBlocked = false;
    public int money = 500;
    public int maxInputs = 5;
    public bool breakingRound = true;
    public AudioSource fixSound;
    public AudioSource failSound;
    public GameObject hammer;
    public AudioSource[] fixSoundList;

    private DateTime? breakingStartTime = null;
    private DateTime? buttonRepairStartTime = null;

    //player ability toggles

    //player status
    public bool isGrounded;
    public bool isJumping;
    public bool isInteracting = false;
    public bool isTriggering = false;
    public bool isFacingRight = false;

    public GameObject triggeredObject;

    //private variable
    private Vector3 _moveDirection = Vector3.zero;
    private CharacterController2D _characterController;

    // Interaction sprites to player 1
    public Sprite sprite0;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        _characterController = GetComponent<CharacterController2D>();
        var manager = GameObject.Find("GameManager");
        manager.GetComponent<InputWatcher>().subscribe(tag, new
          DualShock4Input[] {
          DualShock4Input.SQUARE,
          DualShock4Input.CROSS,
          DualShock4Input.CIRCLE,
          DualShock4Input.TRIANGLE,
          DualShock4Input.R1
          }, (e) => Interact((int)e)
        );

        manager.GetComponent<RoundAnnouncer>().GetComponent<RoundAnnouncer>().subscribe((e) =>
        {
            Debug.Log(e);
            if (e == Round.BREAK)
            {
                // do break round related logic
                isInputBlocked = false;
            }
            else if (e == Round.REPAIR)
            {
                // do repair round related logic
            }
        });

        // GameObject.Find("RoundAnnouncer").GetComponent<RoundAnnouncer>().subscribe((e) => {
        //   Debug.Log(e);
        //   if(e == Round.BREAK) {
        //     // do break logic
        //     isInputBlocked = false;
        //   } else if (e == Round.REPAIR) {
        //     // do repair logic
        //   }
        // });       
    }

    // Update is called once per frame
    void Update()
    {
        if (!isInputBlocked)
        {
            if (!isInteracting)
            {
                Move();
            }
            else
            {
                // interact()
            }
        }

        stateMonitor();
    }

    private void stateMonitor()
    {
        var now = DateTime.Now;

        if (breakingRound
            && isInteracting
            && breakingStartTime.HasValue
            && now.Subtract(breakingStartTime.Value).TotalSeconds > Consts.MAX_BREAKING_TIME)
        {
            if (triggeredObject != null)
            {
                BreakableContainerBehaviour breakableObject = triggeredObject.GetComponent<BreakableContainerBehaviour>();
                FinishBreaking(breakableObject);
            }
        }

        if (!breakingRound
            && isInteracting
            && buttonRepairStartTime.HasValue
            && now.Subtract(buttonRepairStartTime.Value).TotalSeconds > Consts.MAX_BUTTOM_FIXING_TIME)
        {
            if (triggeredObject != null)
            {
                BreakableContainerBehaviour breakableObject = triggeredObject.GetComponent<BreakableContainerBehaviour>();
                CancelFixing(breakableObject);
            }
        }
    }

    private void Move()
    {
        _moveDirection.x =
            Input.GetAxis($"{tag}Horizontal") > 0 && Input.GetAxis($"{tag}Horizontal") < 0.15
            || Input.GetAxis($"{tag}Horizontal") < 0 && Input.GetAxis($"{tag}Horizontal") > -0.15
            ? 0
            : walkSpeed * Input.GetAxis($"{tag}Horizontal");

        if (_moveDirection.x < 0)
        {
            renderer.flipX = true;
            isFacingRight = false;
        }
        else if (_moveDirection.x > 0)
        {
            renderer.flipX = false;
            isFacingRight = true;
        }

        if (isGrounded) //player is grounded
        {
            GroundedMovement();
        }

        _moveDirection.y -= gravity * Time.deltaTime;
        _characterController.move(_moveDirection * Time.deltaTime);
        flags = _characterController.collisionState;
        isGrounded = flags.below;

        if (flags.above)
        {
            _moveDirection.y -= gravity * Time.deltaTime;
        }

        /// Mecanica de interacción ///
        if (isTriggering && Input.GetButtonUp(DualShock4Input.SQUARE.withTag(tag)))
        {
            isInteracting = true;
            var breakableObject = triggeredObject.GetComponent<BreakableContainerBehaviour>();
            breakableObject.interactionSprite.GetComponent<SpriteRenderer>().enabled = false;
            if (breakingRound)
            {
                breakableObject.textSprite.GetComponent<SpriteRenderer>().enabled = true;
                breakingStartTime = DateTime.Now;
            }
            else
            {
                ShowNextFixButton(breakableObject);
                buttonRepairStartTime = DateTime.Now;
            }
        }
    }

    private void ShowNextFixButton(BreakableContainerBehaviour breakableObject)
    {
        var nextExpectedButton = breakableObject.inputOrder[breakableObject.inputOrder.Count - breakableObject.rightCommand - 1];

        switch (nextExpectedButton)
        {
            case 0:
                breakableObject.interactionSprite.GetComponent<SpriteRenderer>().sprite = sprite0;
                break;
            case 1:
                breakableObject.interactionSprite.GetComponent<SpriteRenderer>().sprite = sprite1;
                break;
            case 2:
                breakableObject.interactionSprite.GetComponent<SpriteRenderer>().sprite = sprite2;
                break;
            case 3:
                breakableObject.interactionSprite.GetComponent<SpriteRenderer>().sprite = sprite3;
                break;
        }

        breakableObject.interactionSprite.GetComponent<SpriteRenderer>().enabled = true;
    }

    private void GroundedMovement()
    {
        _moveDirection.y = 0;
        isJumping = false;

        if (Input.GetButtonDown(DualShock4Input.TRIANGLE.withTag(tag)))
        {
            _moveDirection.y = jumpSpeed;
            isJumping = true;
        }
    }

    private void Interact(int input)
    {
        if (triggeredObject != null && isInteracting)
        {
            if (breakingRound)
            {
                Romper(input);
            }
            else
            {
                Reparar(input);
            }
        }
    }

    private void Romper(int input)
    {
        BreakableContainerBehaviour breakableObject = triggeredObject.GetComponent<BreakableContainerBehaviour>();

        if (input != (int)DualShock4Input.R1)
        {
            breakableObject.inputOrder.Add(input);
            playSound(input);
            if (breakableObject.inputOrder.Count >= maxInputs)
            {
                FinishBreaking(breakableObject);
            }
        }
        else if (input == (int)DualShock4Input.R1)
        {
            if (isInteracting)
            {
                FinishBreaking(breakableObject);
            }
        }
    }

    private void FinishBreaking(BreakableContainerBehaviour breakableObject)
    {
        breakableObject.isBroken = true;
        breakableObject.rightCommand = 0;
        isInteracting = false;
        isTriggering = false;
        breakableObject.textSprite.GetComponent<SpriteRenderer>().enabled = false;
        breakingStartTime = null;
        breakableObject.breakSound.Play();
        breakableObject.setChildrenState(true);
    }

    public void Reparar(int input)
    {
        buttonRepairStartTime = DateTime.Now;
        var breakableObject = triggeredObject.GetComponent<BreakableContainerBehaviour>();
        money -= Consts.MONEY_TO_REPAIR;

        if (input == breakableObject.inputOrder[breakableObject.inputOrder.Count - breakableObject.rightCommand - 1])
        {
            breakableObject.rightCommand++;

            if (breakableObject.rightCommand == breakableObject.inputOrder.Count)
            {
                FinishFixing(breakableObject);
            }
            else
            {
                ShowNextFixButton(breakableObject);
            }
            //Audio
            playSound(input);
        }
        else
        {
            CancelFixing(breakableObject);
        }
    }

    private void FinishFixing(BreakableContainerBehaviour breakableObject)
    {
        breakableObject.isBroken = false;
        breakableObject.rightCommand = 0;
        isInteracting = false;
        breakableObject.textSprite.GetComponent<SpriteRenderer>().enabled = false;
        breakableObject.interactionSprite.GetComponent<SpriteRenderer>().enabled = false;
        fixSound.Play();
    }

    private void CancelFixing(BreakableContainerBehaviour breakableObject)
    {
        breakableObject.rightCommand = 0;
        isInteracting = false;
        breakableObject.textSprite.GetComponent<SpriteRenderer>().enabled = false;
        breakableObject.interactionSprite.GetComponent<SpriteRenderer>().sprite = sprite0;
        failSound.Play();
    }

    private void playSound(int input)
    {
        fixSoundList[input].Play();
    }
}
