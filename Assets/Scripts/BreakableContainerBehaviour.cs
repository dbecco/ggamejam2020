﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// BreakableContainer is just like BreakableObject
// the difference is that when it is flagged as broken,  
// it should update children BreakableItems that are injected to set their sprite as broken
public class BreakableContainerBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public bool isBroken = false;
    public PlayerController breakedBy;
    public AudioSource breakSound;
    public List<int> inputOrder;
    public int rightCommand;
    public bool breakingRound = true;
    public GameObject interactionSprite;
    public GameObject textSprite;
    public Sprite brokenImage;
    public Sprite fixedImage;

    public BreakableItem[] breakableItems;

    public Sprite startInteractionSprite;

    void Start()
    {
        interactionSprite.GetComponent<SpriteRenderer>().enabled = false;
        textSprite.GetComponent<SpriteRenderer>().enabled = false;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        /*
            When Trigger, show input cue for starting breaking or repairing. 
         */
        if (!this.isBroken && breakingRound || this.isBroken && !breakingRound)
        {
            var player = other.GetComponentInParent<PlayerController>();
            interactionSprite.GetComponent<SpriteRenderer>().sprite = player.sprite0;
            interactionSprite.GetComponent<SpriteRenderer>().enabled = true;
            player.isTriggering = true;
            player.triggeredObject = this.GetComponent<Transform>().gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponentInParent<PlayerController>().triggeredObject == this.GetComponent<GameObject>())
        {
            other.GetComponentInParent<PlayerController>().isTriggering = false;
            interactionSprite.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    // used to set every breakable child as broken and change sprites 
    public void setChildrenState(bool broken)
    {
        if (breakableItems.Count() > 0)
        {
            foreach (var item in breakableItems)
            {
                item.SetState(broken);
            }
        }
        else
        {
            if (broken)
            {
                this.GetComponent<SpriteRenderer>().sprite = brokenImage;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = fixedImage;
            }
        }
    }

}
