﻿using System.Collections.Generic;
using UnityEngine;

public class BreakableObjectScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite brokenSprite;
    public Sprite fixedSprite;
    public bool isBroken = false;
    public PlayerController breakedBy;
    public AudioClip breakSound;
    public List<int> inputOrder;
    public int rightCommand;
    public bool breakingRound = true;
    
    public SpriteRenderer interactionSprite;
    public SpriteRenderer textSprite;

    void Start(){
        interactionSprite.enabled = false;
        textSprite.enabled = false;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        /*
            When Trigger, show input cue for starting breaking or repairing. 
         */
        if (!this.isBroken && breakingRound || this.isBroken && !breakingRound)
        {
            interactionSprite.enabled = true;

            var player = other.GetComponentInParent<PlayerController>();
            player.isTriggering = true;
            player.triggeredObject = this.GetComponent<Transform>().gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        other.GetComponentInParent<PlayerController>().isTriggering = false;
        interactionSprite.enabled = false;
    }
}
