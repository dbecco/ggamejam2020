﻿using System.Collections.Generic;


namespace Utils {
   public enum DualShock4Input {
     
    NONE = -1,
    SQUARE = 0,
    CROSS = 1, 
    CIRCLE = 2,
    TRIANGLE = 3,
    L1 = 4,
    R1 = 5,
    L2 = 6,
    R2 = 7,
    SHARE = 8,
    OPTIONS = 9,
    L3 = 10,
    R3 = 11,
    PS = 12,
    TOUCHPAD = 13

  }

  public static class DualShock4 {
    private static Dictionary<DualShock4Input, string> _nameMap = new Dictionary<DualShock4Input, string>() {
      {DualShock4Input.SQUARE, "Square"},
      {DualShock4Input.CROSS, "Cross"},
      {DualShock4Input.TRIANGLE, "Triangle"}, 
      {DualShock4Input.CIRCLE, "Circle"},
      {DualShock4Input.R1, "CancelAction"}
    };

    public static string withTag(this DualShock4Input input, string tag) {
      string inputString = null;
      _nameMap.TryGetValue(input, out inputString);

      return $"{tag}{inputString}";
    }

  }
}

