﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Utils
{
    class Consts
    {
        public const int MONEY_TO_REPAIR = 10;
        public const int MAX_BREAKING_TIME = 5;
        public const double MAX_BUTTOM_FIXING_TIME = 1.0;
    }
}
